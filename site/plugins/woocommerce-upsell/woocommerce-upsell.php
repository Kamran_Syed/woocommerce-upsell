<?php

/**
 * Plugin Name: WooCommerce Upsell Plugin
 * Plugin URI: 
 * Description: WooCommerce Upsell Plugin
 * Version: 1.0
 * Author: Agile Solutions PK
 * Author URI:
 */
 if ( !class_exists('Agile_Upsell_Plugin')){
	class Agile_Upsell_Plugin{
				
		function action_woocommerce_before_cart_contents(){
			global  $woocommerce;
			
			$x = WC()->cart->get_cart();
			foreach ( $x as $key => $value ) {
				$p_id = $value['product_id'];
				$terms = get_the_terms( $p_id, 'product_cat' );
				if($terms){
					foreach ($terms as $term) {
						$product_cat_name = $term->slug;
						if($product_cat_name == 'custom-card'){ 
							?>
							<script>
								jQuery( document ).ready(function() {
									jQuery("#agile_sol_add_btn").hide();
								});
							</script>
							<?php
						}
					}
				}
			}
		}
		
		function save_cart_item_meta( $item_id, $values, $cart_item_key){
			$msg = $_SESSION["aspk_wc_message"]; 
			if($msg){
				$id = $values['product_id'];
				$terms = get_the_terms( $id, 'product_cat' );
				if($terms ){
					foreach ($terms as $term) {
						$product_cat_name = $term->slug;
						if($product_cat_name == 'custom-card'){
							wc_add_order_item_meta( $item_id, 'Greeting-Message', $msg);	
						}
					}
				}
			}
			return $values;
		}

		function product_add_to_cart(){
			global $woocommerce;
			
			session_start();
			if(isset($_POST['aspk_121agile_submit121'])){
				$product_id = $_POST['agile_add_item'];
				$wc_msg = $_POST['aspk_wc_desc121'];
				$_SESSION["aspk_wc_message"] = $wc_msg;
				WC()->cart->add_to_cart( $product_id );
			}
			
		}
		
		function wc_images_upsell(  ){ 
			?>
		
		<!-- Large modal -->
		<button type="button" id="agile_sol_add_btn" class="button" data-toggle="modal" data-target=".bs-example-modal-lg" style="background-color:black !important;padding: 0.6em !important;border-radius: 5px !important;">Add Custom Note</button>

			<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
			  <div class="modal-dialog modal-lg">
				<div class="modal-content" style="margin-top:5em;">
					<div class="modal-header">
						<div class="row">
							<div class="col-md-11" ><h2 style="text-align:center;"> Select a card for personal note</h2></div>
							<div class="col-md-1" ><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
						</div>
					</div>
					<form method="post" action="">
						<div class="row"> 
						<?php  
							$args = array( 'post_type' => 'product', 'posts_per_page' => 100000, 'product_cat' => 'custom-card', 'orderby' => 'date','order' => 'DESC' );
							$posts_prod = new WP_Query( $args );
							$all_prod = $posts_prod->posts;
							if($all_prod){
								$n=1;
								foreach($all_prod as $product){ 
								$n++;
									?>
									<div class="col-md-2">
										<?php echo get_the_post_thumbnail( $product->ID ); ?>
										<h3 style="text-align:center;"><input type="radio" <?php if($n == 4){ echo 'checked'; } ?> name="agile_add_item" value="<?php echo $product->ID; ?>"></h3>
									</div>
									<?php
								}
							}
						?>
						</div>
						
						<div class="row">
							<div class="col-md-12" style="text-align:center;padding-top: 0.6em;" >
								<h4>What would you like to say?</h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" style="text-align:center;padding-top: 0.8em;" >
								<textarea value="" name="aspk_wc_desc121"  style="padding-top: 0.3em;width:36em;height:3em;" placeholder="Write here...."></textarea>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" style="text-align:center;padding: 0.6em;">
								<input type="submit" value="Add to Order" class="button" name="aspk_121agile_submit121" style="background-color:#c88419!important;padding: 0.6em !important;border-radius: 5px !important;color:white !important;">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">No Thanks</button>
						</div>
					</form>
				</div>
			  </div>
			</div>


				<script type="text/javascript">
					jquery('#myModal').modal(options)
				</script>
			<?php
		}
		
		function __construct(){
			register_activation_hook( __FILE__, array(&$this, 'install') );
			add_action('admin_menu', array(&$this,'admin_menu_options_page'));
			add_action('admin_enqueue_scripts', array(&$this, 'admin_scripts') );
			add_action('wp_enqueue_scripts', array(&$this, 'frontend_scripts') );
			add_action( 'woocommerce_cart_actions', array( &$this,'wc_images_upsell'), 10, 0 );
			add_action( 'init', array( &$this, 'product_add_to_cart' ) );
			add_filter( 'woocommerce_add_order_item_meta', array( &$this, 'save_cart_item_meta'), 10, 3 );
			add_action( 'woocommerce_before_cart_contents', array( &$this, 'action_woocommerce_before_cart_contents' ),10,0);
			add_action( 'pre_get_posts',array(&$this, 'custom_pre_get_posts_query' ),5);
		}
		
		

		function custom_pre_get_posts_query( &$q ) {

			if ( ! $q->is_main_query() ) return;
			if ( ! $q->is_post_type_archive() ) return;
			
			if ( ! is_admin() && is_shop() ) {

				$q->set( 'tax_query', array(array(
					'taxonomy' => 'product_cat',
					'field' => 'slug',
					'terms' => array( 'custom-card' ), // Don't display products in the knives category on the shop page
					'operator' => 'NOT IN'
				)));
			
			}

			remove_action( 'pre_get_posts', array(&$this, 'custom_pre_get_posts_query' ) );

		}
		
		function install(){
			global $wpdb;
				
			$sql="CREATE TABLE IF NOT EXISTS  `".$wpdb->prefix."wc_upsell_cards` ( 
				`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
				`time` DATETIME NOT NULL ,
				`img_path` varchar(255) DEFAULT NULL,
				`img_url` varchar(255) DEFAULT NULL,
				`wc_sku` varchar(255) DEFAULT NULL
				) ENGINE = InnoDB;";
			$wpdb->query($sql);
		}
		
		function admin_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script( 'jssor', plugins_url( 'js/jssor.js', __FILE__ ),array('jquery'));
			wp_enqueue_script( 'jssor-slider', plugins_url( 'js/jssor.slider.js', __FILE__ ),array('jquery','jssor'));
			wp_enqueue_script('js-fe-bootstrap',plugins_url('js/js-agile-bootstrap.js', __FILE__)
			);
			wp_enqueue_style( 'agile-fe-bootstrap', plugins_url('css/agile-bootstrap.css', __FILE__) );
			
		}
		
		function frontend_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script( 'jssor', plugins_url( 'js/jssor.js', __FILE__ ),array('jquery'));
			wp_enqueue_script( 'jssor-slider', plugins_url( 'js/jssor.slider.js', __FILE__ ),array('jquery','jssor'));
			wp_enqueue_script('jquery-ui-dialog');
			wp_enqueue_style('jquery-ui-css',"//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css");
			wp_enqueue_script('js-fe-bootstrap',plugins_url('js/js-agile-bootstrap.js', __FILE__)
			);
			wp_enqueue_style( 'agile-fe-bootstrap', plugins_url('css/agile-bootstrap.css', __FILE__) );
		}
		
		function admin_menu_options_page(){
			add_menu_page('WooCommerce Upcell', 'WooCommerce Upcell', 'manage_options', 'wooCommerce_upsell', array(&$this, 'add_templates') );
		}
		
		function delete_card_img_by_id($id){
			global $wpdb;
			
			$sql = "DELETE FROM {$wpdb->prefix}wc_upsell_cards where id = '{$id}'" ;//22
			$wpdb->query($sql);	
		}
		
		function insert_cards_img_and_sku($path,$url,$sku){
			global $wpdb;
			
			$date = date('Y-m-d H:i:s');
			$sql = "INSERT INTO {$wpdb->prefix}wc_upsell_cards(time,img_path,`img_url`,`wc_sku`) VALUES('{$date}','{$path}','{$url}','{$sku}')";
			$wpdb->query($sql);
		}
		
		function get_img_path_by_id($id){
			global $wpdb;
			
			$sql = "SELECT img_path FROM {$wpdb->prefix}wc_upsell_cards WHERE id='{$id}'" ;
			return $wpdb->get_var($sql);
		}
		
		function get_img_sku_by_sku($sku){
			global $wpdb;
			
			$sql = "SELECT wc_sku FROM {$wpdb->prefix}wc_upsell_cards WHERE wc_sku='{$sku}'" ;
			return $wpdb->get_var($sql);
		}
		
		function get_all_wc_upsell_card(){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}wc_upsell_cards" ;
			return $wpdb->get_results($sql);
		}
		
		function add_templates(){
			if(isset($_POST['aspk_save_card'])){
				$sku = $_POST['aspk_wc_upsell_sku'];
				$res = $this->get_img_sku_by_sku($sku);
				if(empty($res)){
					if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
					$uploadedfile = $_FILES['aspk_wc_upsell_card'];
					$upload_overrides = array( 'test_form' => false );
					$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
					$path = $movefile['file'];
					$url = $movefile['url'];
					$this->insert_cards_img_and_sku($path,$url,$sku);
				}else{
					echo '<h2>This SKU is already exist please use different SKU </h2>';
				}
			}
			if(isset($_GET['del_img'])){
				$del_id = $_GET['del_img'];
				$path =  $this->get_img_path_by_id($del_id);
				if($path){
					unlink($path);
				}
				$this->delete_card_img_by_id($del_id);
			}
			?>
			<div class="tw-bs container">
				<div class="row">
					<div class="col-md-12"><h2 style="text-align:center;">Add or remove templates</h2></div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form action="" class="form-group" method="post" id="wa_new_company_form" enctype="multipart/form-data"> 
							<div class="row">
								<div class="col-md-12" style="margin-top:1em;">
									<div style="float:left;width:6em;"><b>Upload Card</b></div>
									<div style="float:left;margin-left:1em;"><input type="file" required name="aspk_wc_upsell_card" value=""></div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em;">
									<div style="float:left;width:6em;"><b>SKU</b></div>
									<div style="float:left;margin-left:1em;"><input type="text" required name="aspk_wc_upsell_sku" value=""></div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em;">
									<input type="submit" value="Save Card" name="aspk_save_card" class="btn btn-primary">
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="row">
				<?php
					$all_res = $this->get_all_wc_upsell_card();
					if($all_res){
						foreach($all_res as $res){ ?>
							<div class="col-md-4" style="margin-top:1em;">
								<div style="clear:left;"><img src="<?php echo $res->img_url; ?>" style="width:23em;"></div>
								<div style="clear:left;border: 1px solid;padding:1em;width:23em;"><?php echo $res->wc_sku; ?></div>
								<div style="clear:left;margin-top:1em;text-align:center;">
									<a href="<?php echo admin_url('admin.php?page=wooCommerce_upsell&del_img='.$res->id); ?>"><input type="button" value="Delete" class="btn btn-link" style="background-color:red;border:red;color:white;border-radius:5px;"></a>
								</div>
							</div><?php
						}
					}
					?>
				</div>
			</div>
			<?php
		}
		
	}
}
new Agile_Upsell_Plugin();